package org.shevtsova;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
public class AlertTest {
    WebDriver driver;

    @BeforeClass
    public void createNewInstanceChromeDriver(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void navigateOnUrl() {
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
    }

    @AfterClass
    public void closeBrowser() {
        driver.quit();
    }

    //TEST 1.Натиснути кнопку Click for JS Confirm. В алерті вибрати Оk. Перевірити напис у розділі Result.
    @Test
    public void clickOKForJSConfirm() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
        button.click();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Ok");
    }

    //TEST 2.Обробка натискання на кнопку Click for JS Confirm та вибір Cancel.Кроки ті ж але вибрати Cancel
    @Test
    public void clickCancelForJSConfirm() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
        button.click();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Cancel");
    }

    //TEST 3.Обробка кнопки Click for JS Prompt. Кроки ті ж, що і в попередніх тестах, але перевірити введення тексту в prompt
    @Test
    public void inputValidDataForJSPrompt() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        button.click();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("shevtsova yuliia");
        alert.accept();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: shevtsova yuliia");
    }

    //TEST 4.Кроки ті ж, що і в попередніх тестах, але перевірити без введення тексту в prompt
    @Test
    public void clickOKForJSPrompt() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        button.click();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered:");
    }

    //TEST 5.Кроки ті ж, що і в попередніх тестах, але перевірити введення тексту і натиснути cancel
    @Test
    public void clickCancelWithDataForJSPrompt() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        button.click();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("shevtsova yuliia");
        alert.dismiss();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }

    //TEST 6.Кроки ті ж, що і в попередніх тестах, але перевірити без введення тексту в prompt і натиснути cancel
    @Test
    public void clickCancelWithoutDataForJSPrompt() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        button.click();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }
}
