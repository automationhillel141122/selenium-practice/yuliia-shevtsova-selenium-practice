package org.shevtsova;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;

public class FrameTest {
    WebDriver driver;

    @BeforeClass
    public void createNewInstanceChromeDriver() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void navigateOnUrl() {
       driver.get("https://the-internet.herokuapp.com/nested_frames");
    }

    @Test(dataProvider = "frameGetData")
    public void frameTestWithDataProvider(String nameFrame, String expectedResult){
        if (nameFrame != "frame-bottom") {
            driver.switchTo()
                    .frame("frame-top")
                    .switchTo()
                    .frame(nameFrame);
        } else {
            driver.switchTo().frame(nameFrame);
        }
        String content = driver.findElement(By.xpath("//body")).getText();
        Assert.assertEquals(content, expectedResult);
        driver.switchTo().defaultContent();
    }


    @DataProvider
    public Object[][] frameGetData() {
        return new Object [][] {
                {"frame-middle", "MIDDLE"},
                {"frame-right", "RIGHT"},
                {"frame-left", "LEFT"},
                {"frame-bottom", "BOTTOM"}
        };
    }
    @AfterClass
    public void afterClass() {
        driver.quit();
    }
}