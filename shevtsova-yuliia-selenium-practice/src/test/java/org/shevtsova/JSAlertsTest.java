package org.shevtsova;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class JSAlertsTest {

    WebDriver driver;

    @BeforeClass
    public void createNewInstanceChromeDriver() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void navigateOnUrl() {

        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
    }

    @AfterClass
    public void closeBrowser() {

        driver.quit();
    }

    //TEST 1.1.JavascriptExecutor. Натиснути кнопку Click for JS Confirm. В алерті вибрати Оk. Перевірити напис у розділі Result.
    @Test
    public void clickOKForJSConfirmJavaScriptExecutor() {

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsConfirm();";
        javascriptExecutor.executeScript(script);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Ok");

    }

    //TEST 1.2.JavascriptExecutor.Обробка натискання на кнопку Click for JS Confirm та вибір Cancel.Кроки ті ж але вибрати Cancel
    @Test
    public void clickCancelForJSConfirmJavaScriptExecutor() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsConfirm();";
        javascriptExecutor.executeScript(script);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Cancel");
    }

    //TEST 1.3.JavaScriptExecutor. Обробка кнопки Click for JS Prompt. Кроки ті ж, що і в попередніх тестах, але перевірити введення тексту в prompt
    @Test
    public void inputValidDataForJSPromptJavaScriptExecutor() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsPrompt();";
        javascriptExecutor.executeScript(script);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("shevtsova yuliia");
        alert.accept();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: shevtsova yuliia");
    }

    //TEST 1.4.JavaScriptExecutor. Кроки ті ж, що і в попередніх тестах, але перевірити без введення тексту в prompt
    @Test
    public void clickOKForJSPromptJavaScriptExecutor() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsPrompt();";
        javascriptExecutor.executeScript(script);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered:");
    }

    //TEST 1.5.JavaScriptExecutor. Кроки ті ж, що і в попередніх тестах, але перевірити введення тексту і натиснути cancel
    @Test
    public void clickCancelWithDataForJSPromptJavaScriptExecutor() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsPrompt();";
        javascriptExecutor.executeScript(script);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("shevtsova yuliia");
        alert.dismiss();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }

    //TEST 1.6.JavaScriptExecutor. Кроки ті ж, що і в попередніх тестах, але перевірити без введення тексту в prompt і натиснути cancel
    @Test
    public void clickCancelWithoutDataForJSPromptJavaScriptExecutor() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsPrompt();";
        javascriptExecutor.executeScript(script);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }

    //TEST 2.1.JavascriptExecutor. Натиснути кнопку Click for JS Confirm. В алерті вибрати Оk. Перевірити напис у розділі Result.
    @Test
    public void clickOKForJSConfirmByJS() {

        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Ok");

    }

    //TEST 2.2.JavascriptExecutor.Обробка натискання на кнопку Click for JS Confirm та вибір Cancel.Кроки ті ж але вибрати Cancel
    @Test
    public void clickCancelForJSConfirmByJS() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Cancel");
    }

    //TEST 2.3.JavaScriptExecutor. Обробка кнопки Click for JS Prompt. Кроки ті ж, що і в попередніх тестах, але перевірити введення тексту в prompt
    @Test
    public void inputValidDataForJSPromptByJS() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("shevtsova yuliia");
        alert.accept();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: shevtsova yuliia");
    }

    //TEST 2.4.JavaScriptExecutor. Кроки ті ж, що і в попередніх тестах, але перевірити без введення тексту в prompt
    @Test
    public void clickOKForJSPromptByJS() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered:");
    }

    //TEST 2.5.JavaScriptExecutor. Кроки ті ж, що і в попередніх тестах, але перевірити введення тексту і натиснути cancel
    @Test
    public void clickCancelWithDataForJSPromptByJS() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("shevtsova yuliia");
        alert.dismiss();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }

    //TEST 2.6.JavaScriptExecutor. Кроки ті ж, що і в попередніх тестах, але перевірити без введення тексту в prompt і натиснути cancel
    @Test
    public void clickCancelWithoutDataForJSPromptByJS() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);

        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();
        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }
}
